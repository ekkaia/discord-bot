require("dotenv").config();
const Discord = require("discord.js");
const mongoose = require("mongoose")
const bot = new Discord.Client();
const prefix = "!";
const token = process.env.token;

// MONGODB
mongoose.connect(process.env.mongo, { useNewUrlParser: true });

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function () {
	console.log("Connexion établie");
});

const characterSchema = new mongoose.Schema({
	color: String,
	title: String,
	description: String,
	affiliation: String,
	saison1: String,
	statut: String
});

const Character = mongoose.model("Character", characterSchema);

const imperial = "#d50000";
const justiceAllies = "#ffd600";
const dragunite = "#aa00ff";
const darklord = "#aa00ff";
const laval = "#dd2c00";
const hawker = "#00bfa5";

var MedossCharacter = new Character({
	color: laval,
	title: "Marxan_Clume",
	description: "Marxan Clume est un laval de la forêt de feu, connu pour son goût de l'exploration et son idéalisme. C'est l'oncle de Thaktan Clume.",
	affiliation: "Laval",
	saison1: "Face à la diminution croissante des flammes de la forêt de feu Laval, il se rendit dans les Terres désolées et recontra __Kayenn__, le Grand Forgeron. Ce dernier lui indiqua où se trouvait __Ha' Dligen__, le dragon à l'origine des flammes de la forêt de feu, et lui forgea un sceptre capable de cautériser ses blesssures. Par la suite, Marxan se rendit à Dragun' Daar et, conscient de sa réputation de rêveur parmi les Lavals, échangea à __Arshan__ des armes en cendragon contre le recrutement par ses Mordeurs de Lavals pour un grand projet. Pendant plusieurs mois, Marxan fit travailler les Lavals à soigner Ha' Dligen. Finalement, l'irruption des Émissaires provoqua le réveil du dragon, qui fit exploser le Neshnan en prenant son envol. Marxan, désespéré par son échec, la mort de la plupart des Lavals dans le Neshnan et les critiques de certains Émissaires, mit fin à ses jours.",
	statut: "Mort"
})
/*
MedossCharacter.save(function(error) {
	console.log("saved!");
	if (error) {
		console.error(error);
	}
});*/

// DISCORD BOT
bot.login(token);

bot.on("ready", () => {
	bot.user.setActivity("Type !list for commands");
});

bot.on("message", message => {
	const args = message.content.slice(prefix.length).split(" ");
	const command = args.shift().toLowerCase();

	if (command === "char") {
		Character.find({ title : args[0] }, (err, characters) => {
			if (err) {
				return console.error(err);
			} else {
				message.channel.send(new Discord.RichEmbed()
					.setColor(characters[0].color)
					.setTitle(characters[0].title)
					.setDescription(characters[0].description)
					.addField("Affiliation(s)", characters[0].affiliation)
					.addField("Saison 1", characters[0].saison1, true)
					.addField("Statut", characters[0].statut, true));
			}
		});
	} else if (command === "list") {
		Character.find((err, characters) => {
			if (err) {
				return console.error(err);
			} else {
				var characterList = "__**Liste des personnages :**__\n";
				for (character of characters) {
					characterList += `${prefix}char ${character.title}\n`;
				}
                
                characterList += `\n__**Autres commandes :**__\n${prefix}zerato [personnages]\n${prefix}krakodil [numéro du jour]`;
				message.reply(characterList);
			}
		});
	} else if (command === "zerato") {
        if (args.length >= 2) {
            var random = Math.floor(Math.random() * Math.floor(args.length));
            var zerato = args[random];
            message.reply(`Zérato, c'est ${zerato} !`);
        } else {
            message.reply(`Tu n'as pas saisi assez d'arguments !`)
        }
    } else if (command === "krakodil") {
        if (args.length === 1 && args[0] > 0) {
            message.reply(`Le cours de la krakodil est actuellement de ${Math.floor(Math.cos(args[0])*15+40)} Sabliers de cornaline.`);
        } else {
            message.reply(`Argument incorrect : entrez un jour !`)
        }
    }
});